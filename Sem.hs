data Term = Var String
                | Abs String Term
                | App Term Term
                deriving Show
 
type Context = [(String, Term)]
 
show' :: Term -> String
show' (Var a) = a
show' (Abs x e) = "(λ " ++ x ++ ". " ++ show' e ++ ")"
show' (App e1 e2) = "(" ++ show' e1 ++ " " ++ show' e2 ++ ")"
 
eval :: Context -> Term -> Term
eval context e@(Var a) = maybe e id (lookup a context)
eval context (Abs x e) = Abs x (eval context e)
eval context (App e1 e2) = apply context (eval context e1) (eval context e2)
 
apply :: Context -> Term -> Term -> Term
apply context (Abs x e) e2 = eval ((x, e2):context) e
apply context e1 e2 = App e1 e2
 
parse :: String -> (Term, String)
parse ('(':'λ':s) = let
    (Var a, s') = parse (tail s)
    (e, s'') = parse (drop 2 s')
    in (Abs a e, tail s'')
 
parse ('(':s) = let
    (e1, s') = parse s
    (e2, s'') = parse (tail s')
    in (App e1 e2, tail s'')
 
parse (c:s) | elem c " .)" = (Var "", c:s)
            | otherwise    = let ((Var a), s') = parse s
                             in (Var (c:a), s')
 
run :: String -> String
run = show' . eval [] . fst . parse
main = do
  line <- getLine
  putStrLn$ run line